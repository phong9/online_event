/* スムーススクロール */
var scrollElm = (function () {
  if ('scrollingElement' in document) return document.scrollingElement;
  if (navigator.userAgent.indexOf('WebKit') != -1) return document.body;
  return document.documentElement;
})();
(function () {
  var duration = 500;
  var ignore = '.noscroll';
  var easing = function (t, b, c, d) { return c * (0.5 - Math.cos(t / d * Math.PI) / 2) + b; }; //jswing
  var smoothScrollElm = document.querySelectorAll('a[href^="#"]:not(' + ignore + ')');
  Array.prototype.forEach.call(smoothScrollElm, function (elm) {
    elm.addEventListener('click', function (e) {
      e.preventDefault();
      var targetElm = document.querySelector(elm.getAttribute('href'));
      if (!targetElm) return;
      var targetPos = targetElm.getBoundingClientRect().top;
      var startTime = Date.now();
      var scrollFrom = scrollElm.scrollTop;
      (function loop() {
        var currentTime = Date.now() - startTime;
        if (currentTime < duration) {
          scrollTo(0, easing(currentTime, scrollFrom, targetPos, duration));
          window.requestAnimationFrame(loop);
        } else {
          scrollTo(0, targetPos + scrollFrom);
        }
      })();
    })
  });
})();

//custom func
function adjustIssueImgHeight() {
  if ($(window).width() <= 767) {
    $('#issue_bg1').height($('.issue_wrap').innerHeight());
  } else {
    $('#issue_bg1').height('100%');
  }
}

$(function () {
  adjustIssueImgHeight();
});

$(window).resize(function () {
  adjustIssueImgHeight();
});

$('.q_question').on('click', function (e) {
  $(this).closest('.question_content').find('.q_answer').slideToggle();
  $(this).find('.q_minus_ico').toggle();
  $(this).find('.q_plus_ico').toggle();
});

$('a[href^="#"]').on('click', function () {
  var headerHight = 80;
  var href = $(this).attr('href');
  var target = $(href == '#' || href == '' ? 'html' : href);
  var position = target.offset().top - headerHight;
  $('body').removeClass('open');
  $('body, html').animate({ scrollTop: position }, 1000, 'swing');
  return false;
});

// モーダルウィンドウメニューのjQuery
// 開閉用ボタンをクリックでクラスの切替え
$('#js__btn').on('click', function () {
  $('body').toggleClass('open');
});

// メニュー名以外の部分をクリックで閉じる
$('#js__nav').on('click', function () {
  $('body').removeClass('open');
});


// よくある質問

$('.q-question').on('click', function () {
  $(this).closest('.q-container').find('.q-answer').slideToggle();
  $(this).find('.q-plus-icon').toggle();
  $(this).find('.q-cross-icon').toggle();
});

$('#q-open-all').on('click', function () {
  $('.q-answer').slideDown();
  $('.q-plus-icon').hide();
  $('.q-cross-icon').show();
  $('#q-close-all').show();
  $(this).hide();
});

$('#q-close-all').on('click', function () {
  $('.q-answer').slideUp();
  $('.q-plus-icon').show();
  $('.q-cross-icon').hide();
  $('#q-open-all').show();
  $(this).hide();
});
